package pl.sda.figury;

public class Okreg extends Figura{

    protected final static double PI = 3.14; //static wszystkie obiekty będą miały współdzielone PI
    protected double r; //r=promien

    public Okreg(double r) {
        this.r = r;
    }

     @Override
    public double obliczObwod() {
        return 2*PI*r;
    }

    @Override
    public double obliczPole() {
        return PI*r*r;
    }
}
